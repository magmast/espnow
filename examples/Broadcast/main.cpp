#include <array>

#include <Arduino.h>
#include <EspNow.h>

void setup()
{
    Serial.begin(9600);
    randomSeed(0);

    if (!EspNow.begin())
    {
        Serial.println("Failed to init ESP-NOW");
        return;
    }
}

void loop()
{
    const uint8_t number = random(0, 255);

    // May be called with `const uint8_t*` and `size_t` or with any container
    // that provides `const uint8_t* data()` and `size()` methods, like
    // `std::array<uint8_t, 8>` or `std::vector<uint8_t>`.
    if (!EspNow.broadcast({&number, &number + 1}))
    {
        Serial.println("Failed to broadcast a number");
    }

    delay(1000);
}