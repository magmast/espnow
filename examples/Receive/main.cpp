#include <array>

#include <Arduino.h>
#include <EspNow.h>

void handleReceive(
    const etl::array_view<uint8_t> &mac, const etl::array_view<uint8_t> &data)
{
    Serial.printf("Received number: %d\n", data[0]);
}

void setup()
{
    Serial.begin(9600);

    if (!EspNow.begin())
    {
        Serial.println("Failed to init ESP-NOW");
        return;
    }

    EspNow.onReceive(&handleReceive);
}

void loop()
{
    // Nothing to be done here.
}