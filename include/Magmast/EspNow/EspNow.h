#ifndef MAGMAST_ESP_NOW_ESP_NOW_H
#define MAGAMST_ESP_NOW_ESP_NOW_H

#include <cstddef>
#include <cstdint>

#include <esp_now.h>

#include <array>
#include <functional>

#include <etl/array_view.h>
#include <etl/optional.h>

#include "Peer.h"

namespace Magmast
{
    namespace EspNow
    {
        class EspNow
        {
        private:
            static EspNow _instance;

            static void _handleReceive(const uint8_t *mac, const uint8_t *payload, int size);
            static void _handleSend(const uint8_t *mac, esp_now_send_status_t status);

        public:
            using ReceiveCallback = std::function<void(const Address &mac, const etl::array_view<uint8_t> &payload)>;

            static EspNow &getInstance();

            bool begin() const;

            bool send(const Peer &peer, const etl::array_view<uint8_t> &payload);

            bool broadcast(const etl::array_view<uint8_t> &payload);

            void onReceive(const ReceiveCallback &&callback);

            void removeOnReceive();

        private:
            etl::optional<ReceiveCallback> _receiveCallback;
            bool _isSending{false};
            uint32_t _lastSentTime = 0;

            bool _send(const Peer &peer, const uint8_t *payload, size_t size);
            void _throttle();
            bool _addPeer(const Peer &peer) const;
        };
    }
}

#endif